import { MouseEvent, useRef } from "react";

export type CriteriaProps = {
  name: string;
  onChange: (e: number) => void;
};

export const Criteria = (props: CriteriaProps) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const starRefs = useRef<HTMLDivElement[]>([]);

  const handleClick = (e: MouseEvent): void => {
    const target = e.currentTarget as HTMLElement;
    console.log(e.currentTarget);
    const value = target.getAttribute("data-index");

    starRefs.current.forEach((star) => {
      star.classList.remove("selected");
    });

    setTimeout(() => {
      target.classList.add("selected");
      switch (Number(value)) {
        case 1:
          target.classList.add("selected--bad");
          break;
        case 2:
          target.classList.add("selected--mediocre");
          break;
      }
    }, 1);

    props.onChange(Number(value));
  };

  return (
    <div className="criteria">
      <h3>{props.name}</h3>
      <div className="star-row star-row--criteria" ref={containerRef}>
        {[...Array<number>(5)].map((_, index: number) => {
          return (
            <div
              key={index}
              className="star-wrapper"
              onClick={handleClick}
              ref={(el) => (starRefs.current[index] = el as HTMLDivElement)}
              data-index={5 - index}
            >
              <svg viewBox="0 0 58 53" className="svg-icon star">
                <defs>
                  <mask id="mstar">
                    <use fill="white" xlinkHref="#star"></use>
                  </mask>
                </defs>
                <rect
                  x="0"
                  y="0"
                  height="100%"
                  width="0%"
                  fill="transparent"
                  mask="url(#mstar)"
                />
                <use
                  xlinkHref="#star"
                  fill="transparent"
                  stroke="black"
                  strokeWidth="2"
                ></use>
              </svg>
            </div>
          );
        })}
      </div>
    </div>
  );
};
