import { useState } from "react";
import "./App.scss";
import { Criteria } from "./Criteria";
import { Total } from "./Total";

function App() {
  const [marks, setMarks] = useState<number[]>([0, 0, 0, 0]);

  const handleStarClick = (e: number, index: number): void => {
    const newMarks = [...marks];
    newMarks[index] = e;
    setMarks(newMarks);
  };

  const isTotal = (): boolean => {
    return marks.indexOf(0) === -1;
  };

  console.log(isTotal());
  return (
    <div className="app">
      <div className="grid">
        <Criteria name="Design" onChange={(e) => handleStarClick(e, 0)} />
        <Criteria
          name="Fonctionnement"
          onChange={(e) => handleStarClick(e, 1)}
        />
        <Criteria name="Animations" onChange={(e) => handleStarClick(e, 2)} />
        <Criteria
          name="Feeling général"
          onChange={(e) => handleStarClick(e, 3)}
        />
      </div>

      {!!isTotal() && (
        <Total
          score={
            marks.reduce((acc, curr) => {
              return acc + curr;
            }, 0) / 4
          }
        />
      )}
    </div>
  );
}

export default App;
