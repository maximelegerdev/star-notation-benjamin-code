import { useEffect, useRef, useState } from "react";

export type TotalProps = {
  score: number;
};

export const Total = (props: TotalProps) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const starRefs = useRef<HTMLDivElement[]>([]);

  const [starsFilling, setStarsFilling] = useState([0, 0, 0, 0, 0]);

  useEffect(() => {
    const marks = [0, 0, 0, 0, 0];

    const starColors = (): void => {
      const stars = starRefs.current;
      stars.forEach((star) => {
        star.classList.remove("bad", "mediocre");
      });
      if (props.score < 2) {
        stars.forEach((star) => {
          star.classList.add("bad");
        });
      } else if (props.score < 3 && props.score >= 2) {
        stars.forEach((star) => {
          star.classList.add("mediocre");
        });
      }
    };

    setStarsFilling(
      marks.map((_: number, index: number) => {
        return Math.max(0, Math.min(props.score - index, 1)) * 100;
      }),
    );

    starColors();
  }, [props.score, setStarsFilling]);

  return (
    <div className="total">
      <h3>{`Total (${props.score})`}</h3>
      <div className="star-row star-row--total" ref={containerRef}>
        {[...Array<number>(5)].map((_, index: number) => {
          return (
            <div
              key={index}
              className="star-wrapper"
              ref={(el) => (starRefs.current[index] = el as HTMLDivElement)}
            >
              <svg viewBox="0 0 58 53" className="svg-icon star">
                <defs>
                  <mask id="mstar">
                    <use fill="white" xlinkHref="#star"></use>
                  </mask>
                </defs>
                <rect
                  x="0"
                  y="0"
                  height="100%"
                  style={{ width: `${starsFilling[4 - index]}%` }}
                  fill="transparent"
                  mask="url(#mstar)"
                />
                <use
                  xlinkHref="#star"
                  fill="transparent"
                  stroke="black"
                  strokeWidth="2"
                ></use>
              </svg>
            </div>
          );
        })}
      </div>
    </div>
  );
};
